package com.kravanh;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.kravanh.model.Model;
import com.kravanh.service.ShapService;

public class Demo{
	
	public static void main(String[] args) {
		ApplicationContext ctx=new ClassPathXmlApplicationContext("web.xml");
//		ShapService shap= ctx.getBean("shapService",ShapService.class);
//		shap.barear("eyjasdlkfjashdgasiodjfklwelasdkjfashfe");
		System.out.println(new Model("Employees").Where("id","!=","1").Where("email", "pangsoramdepo@gmail.com").OrderBy("id", "ASC").Take(2).Get());
		System.out.println(new Model("Employee").Where("id", "1").Count());
		System.out.println(new Model("Employee").Where("id","=", "2").Delete());
//		System.out.println(new Model("Employee").Where("id","=", "2").Update();
	}
}
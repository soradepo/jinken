package com.kravanh.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.kravanh.annotations.KvAuthorize;

@Aspect
@Component
public class KvAuthorizeAspect {
	  //public Object handle(ProceedingJoinPoint joinPoint, KvAuthorize delegate) throws Exception {
	  @Around(value = "@annotation(anno)", argNames = "jp, anno") // aspect method who have the annotation @Delegate
	  public Object handle(ProceedingJoinPoint joinPoint, KvAuthorize delegate) throws Throwable {
		System.out.println("KvAuthorizeAspect Execute!!!");
		System.out.println("Value: "+delegate.value());
		System.out.println("Parameter: "+joinPoint.getArgs()[0]);
	    //Object obj = joinPoint.getThis(); // get the object
	    //Method method = ((MethodSignature) joinPoint.getSignature()).getMethod(); // get the origin method
	    //Method target = obj.getClass().getMethod(delegate.value(), method.getParameterTypes()); // get the delegate method
	    //System.out.println("Method Name: "+method.getName());
    	//return target.invoke(obj, joinPoint.getArgs()); // invoke the delegate method
	    return joinPoint.proceed();
	  }
}

package com.kravanh.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogTrace {
	
	@Before("execution(public String getName())")
	public void LogRun()
	{
		System.out.println("Aspect Run!!!");
	}

}

package com.kravanh.model;

/**
 * 
 * @Author PANG-SORAM-DEPO
 *
 */
public class Model {
	private static String table_name;
	private static String sql = "";
	private boolean where = false;

	// Contructor
	public Model(String Table_name) {
		table_name = Table_name;
		sql = "SELECT * FROM " + table_name;
	}

	public static Model Name(String Table_name) {
		return new Model(table_name);
	}

	public Model Where(String column, String value) {
		if (where)
			sql += " AND " + column + "='" + value + "'";
		else
			sql += " WHERE " + column + "='" + value + "'";
		where = true;
		return this;
	}

	public Model Where(String column, String expression, String value) {
		if (where)
			sql += " AND " + column + expression + "'" + value + "'";
		else
			sql += " WHERE " + column + expression + "'" + value + "'";
		where = true;
		return this;
	}

	public String Delete() {
		sql = "DELETE" + sql.substring(8, sql.length());
		return sql;
	}

	public String Update(String []st) {
		return sql;
	}

	public String First() {
		sql += " LIMIT 1";
		return sql;
	}

	public String Count() {
		sql = sql.substring(0, 7) + "COUNT(*)" + sql.substring(8, sql.length());
		return sql;
	}

	public String Max(String column) {
		sql = sql.substring(0, 7) + "MAX(" + column + ")" + sql.substring(8, sql.length());
		return sql;
	}

	public Model OrderBy(String column, String by) {
		sql += " ORDER BY " + column + " " + by;
		return this;
	}

	public Model Take(int get) {
		sql += " LIMIT " + get;
		return this;
	}

	public String Get() {
		return sql;
	}
}

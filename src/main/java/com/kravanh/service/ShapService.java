package com.kravanh.service;

import com.kravanh.annotations.KvAuthorize;
import com.kravanh.model.Circle;
import com.kravanh.model.Trangle;

public class ShapService {
	private Circle circle;
	private Trangle trangle;

	public Circle getCircle() {
		return circle;
	}

	public void setCircle(Circle circle) {
		this.circle = circle;
	}

	public Trangle getTrangle() {
		return trangle;
	}

	public void setTrangle(Trangle trangle) {
		this.trangle = trangle;
	}

	@KvAuthorize("b")
	public void a(int i) {
		System.out.println("a: " + i);
	}

	@KvAuthorize("a")
	public void b(String i) {
		System.out.println("b: " + i);
	}
	
	@KvAuthorize(value = "MS000001")
	public void barear(String barear) {
		System.out.println("baere execution!!!");
	}
}
